package io.github.alkyaly.timeinabottle.entity;

import io.github.alkyaly.timeinabottle.TimeInABottle;
import org.joml.Quaternionf;
import org.joml.Vector4f;

import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3i;

public class AcceleratorEntityRenderer extends EntityRenderer<AcceleratorEntity> {

    private static final Identifier RING_TEXTURE = TimeInABottle.id("textures/time_ring.png");

    public AcceleratorEntityRenderer(EntityRendererFactory.Context ctx) {
        super(ctx);
    }

    @Override
    public void render(AcceleratorEntity entity, float yaw, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light) {
        super.render(entity, yaw, tickDelta, matrices, vertexConsumers, light);
        VertexConsumer consumer = vertexConsumers.getBuffer(RenderLayer.getEntityTranslucent(this.getTexture(entity)));
        matrices.push();
        matrices.translate(0.5, 0.5, 0.5);
        
        Vector4f vec1, vec2, vec3, vec4;
        for (Direction dir : Direction.values()) {
            matrices.push();
            Vec3i dirVector = dir.getVector();
            float angle = entity.age+tickDelta;
            matrices.multiply(new Quaternionf(dirVector.getX(), dirVector.getY(), dirVector.getZ(), angle));
            MatrixStack.Entry entry = matrices.peek();

            float offset = 0.5001f * (dir.getDirection() == Direction.AxisDirection.NEGATIVE ? -1 : 1); // 0.5001 is to prevent Z-fighting
            if (dir.getAxis() == Direction.Axis.X) {
                vec1 = new Vector4f(offset, -0.5f, -0.5f, 1.0f);
                vec2 = new Vector4f(offset,  0.5f, -0.5f, 1.0f);
                vec3 = new Vector4f(offset,  0.5f,  0.5f, 1.0f);
                vec4 = new Vector4f(offset, -0.5f,  0.5f, 1.0f);
            } else if (dir.getAxis() == Direction.Axis.Y) {
                vec1 = new Vector4f(-0.5f, offset, -0.5f, 1.0f);
                vec2 = new Vector4f(-0.5f, offset,  0.5f, 1.0f);
                vec3 = new Vector4f( 0.5f, offset,  0.5f, 1.0f);
                vec4 = new Vector4f( 0.5f, offset, -0.5f, 1.0f);
            } else {
                vec1 = new Vector4f(-0.5f, -0.5f, offset, 1.0f);
                vec2 = new Vector4f(-0.5f,  0.5f, offset, 1.0f);
                vec3 = new Vector4f( 0.5f,  0.5f, offset, 1.0f);
                vec4 = new Vector4f( 0.5f, -0.5f, offset, 1.0f);
            }

            vec1.mul(entry.getPositionMatrix());
            vec2.mul(entry.getPositionMatrix());
            vec3.mul(entry.getPositionMatrix());
            vec4.mul(entry.getPositionMatrix());
            
            int frame;
            if (entity.getTimeRate() >= 32) {
                frame = 5;
            } else if (entity.getTimeRate() >= 16) {
                frame = 4;
            } else if (entity.getTimeRate() >= 8) {
                frame = 3;
            } else if (entity.getTimeRate() >= 4) {
                frame = 2;
            } else if (entity.getTimeRate() >= 2) {
                frame = 1;
            } else {
                frame = 0;
            }
            
            float minU = (frame)/6f;
            float maxU = (frame+1)/6f;
            
            consumer.vertex(vec1.x(), vec1.y(), vec1.z()).color(1.0f, 1.0f, 1.0f, 1.0f).texture(minU, 1.0f)
                .overlay(OverlayTexture.DEFAULT_UV).light(15728880).normal(dirVector.getX(), dirVector.getY(), dirVector.getZ()).next();
            consumer.vertex(vec2.x(), vec2.y(), vec2.z()).color(1.0f, 1.0f, 1.0f, 1.0f).texture(minU, 0.0f)
                .overlay(OverlayTexture.DEFAULT_UV).light(15728880).normal(dirVector.getX(), dirVector.getY(), dirVector.getZ()).next();
            consumer.vertex(vec3.x(), vec3.y(), vec3.z()).color(1.0f, 1.0f, 1.0f, 1.0f).texture(maxU, 0.0f)
                .overlay(OverlayTexture.DEFAULT_UV).light(15728880).normal(dirVector.getX(), dirVector.getY(), dirVector.getZ()).next();
            consumer.vertex(vec4.x(), vec4.y(), vec4.z()).color(1.0f, 1.0f, 1.0f, 1.0f).texture(maxU, 1.0f)
                .overlay(OverlayTexture.DEFAULT_UV).light(15728880).normal(dirVector.getX(), dirVector.getY(), dirVector.getZ()).next();
            matrices.pop();
        }
        matrices.pop();
    }

    @Override
    public Identifier getTexture(AcceleratorEntity entity) {
        return RING_TEXTURE;
    }
}