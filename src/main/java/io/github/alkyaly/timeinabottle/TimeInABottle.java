package io.github.alkyaly.timeinabottle;

import io.github.alkyaly.timeinabottle.entity.AcceleratorEntity;
import io.github.alkyaly.timeinabottle.item.TimeInABottleItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.ItemGroups;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;

public class TimeInABottle implements ModInitializer {

    public static final String MOD_ID = "timeinabottle";
    public static final Logger LOGGER = LogManager.getLogger("Time in a Bottle");
    public static ModConfig config = new ModConfig();

    public static final TimeInABottleItem TIME_IN_A_BOTTLE = new TimeInABottleItem(new FabricItemSettings().maxCount(1).rarity(Rarity.UNCOMMON));
    public static final EntityType<AcceleratorEntity> ACCELERATOR = Registry.register(
        Registries.ENTITY_TYPE,
        id("accelerator"),
        FabricEntityTypeBuilder.<AcceleratorEntity>create(SpawnGroup.MISC, AcceleratorEntity::new)
                .dimensions(EntityDimensions.fixed(.1f, .1f)).build()
    );

    @Override
    public void onInitialize() {
        Registry.register(Registries.ITEM, id("time_in_a_bottle"), TIME_IN_A_BOTTLE);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS).register(entries -> entries.add(new ItemStack(TIME_IN_A_BOTTLE)));
    }

    public static Identifier id(String path) {
        return new Identifier(MOD_ID, path);
    }
}
